from app import create_app, db
from app.models import Sheet

app = create_app()


@app.shell_context_processor
def make_shell_context():
    return {'db': db, 'Sheet': Sheet} 


if __name__ == '__main__':
    app.run(host=app.config["HOST"], port=app.config["PORT"])
