import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from dotenv import load_dotenv
import atexit

from app.services.monitor import PingHost

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

load_dotenv()

db = SQLAlchemy(
    session_options={
        "autoflush": False,
        "autocommit": False,
        "expire_on_commit": False,
    })

migrate = Migrate()


def create_app():
    from . import routes, models, config

    # prevent multiple poll jobs from running
    if os.environ.get("WERKZEUG_RUN_MAIN") == "true":
        atexit.register(PingHost('126.10.240.20').start().stop)

    print('basedir', BASE_DIR)
    # config
    app = Flask(__name__)
    app.config.from_object(config.Config)
    app.config["UPLOAD_FOLDER"] = os.path.join(BASE_DIR, 'images')
    app.config["STATIC_FOLDER"] = os.path.join(BASE_DIR, 'static')
    app.config["HOST"] = os.getenv("HOST")
    app.config["PORT"] = os.getenv("PORT")
    app.config["CAM_IP"] = '126.10.240.20:8081'
    app.config["CAM_PORT"] = '8081'
    # create directories if they do not exist
    if not os.path.exists(app.config['UPLOAD_FOLDER']):
        os.mkdir(app.config['UPLOAD_FOLDER'])
    if not os.path.exists(app.config['STATIC_FOLDER']):
        os.mkdir(app.config['STATIC_FOLDER'])
    # inits
    db.init_app(app)
    migrate.init_app(app, db)
    routes.init_app(app)
    return app
