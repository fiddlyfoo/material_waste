import time
import subprocess
import logging
import os
from datetime import datetime, timedelta
from concurrent.futures import ThreadPoolExecutor

from logging.handlers import RotatingFileHandler

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
LOG_FILE = os.path.join(BASE_DIR, 'ping.log')

logger = logging.getLogger(__file__)
logger.setLevel(logging.INFO)
rh = RotatingFileHandler(LOG_FILE, maxBytes=1e+7, backupCount=5)
fmt = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
rh.setFormatter(fmt)
logger.addHandler(rh)


class PingHost:
    def __init__(self, host, interval=1):
        self.host = host
        self.interval = interval
        self.ex = ThreadPoolExecutor(1)
        self.future = None
        self.run = False

    def stop(self):
        try:
            if self.future is not None:
                logger.info('stop ping worker')
                self.run = False
                while not self.future.done():
                    time.sleep(1)
        except Exception as e:
            logger.exception(str(e))
        return self

    def start(self):
        try:
            if self.future is None:
                logger.info('start ping worker')
                self.run = True
                self.future = self.ex.submit(self.poll)
        except Exception as e:
            logger.exception(str(e))
        return self

    def poll(self):
        try:
            up = None
            polls = 0
            report_period = timedelta(minutes=1)
            next_report = datetime.now()
            logger.info('begin polling')
            while self.run:
                # ping device
                command = ['ping', '-c', '1', self.host]
                process = subprocess.Popen(
                    command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                out, err = process.communicate()
                succ = process.returncode == 0
                polls += 1
                # log host state change
                if not succ and (up or up is None):
                    logger.info(f'{self.host} .. DOWN.')
                    polls = 0
                    next_report = datetime.now() + report_period
                elif succ and (not up or up is None):
                    logger.info(f'{self.host} .. UP.')
                    polls = 0
                    next_report = datetime.now() + report_period
                # log status at report period
                elif datetime.now() > next_report:
                    logger.info(
                        f'{self.host} .. {"UP" if succ else "DOWN"} '
                        f'(polls {polls})')
                    next_report = datetime.now() + report_period
                up = succ
                time.sleep(self.interval)
            logger.info(f'end polling (polls {polls})')
        except Exception as e:
            logger.exception(str(e))
