from app import db
from datetime import datetime
from pathlib import Path
from sqlalchemy import event
from uuid import uuid4
from flask import url_for, current_app as app
import paramiko
import shutil
import os
import cv2
import json
import app.image_proc as ip
from app import BASE_DIR


class Sheet(db.Model):
    SM_SIDE_IN = 48
    LG_SIDE_IN = 96
    MTL_WEIGHT = {"alum_080": 34.90, "alum_063": 27.24, }

    id = db.Column(db.Integer, primary_key=True)
    created_on = db.Column(db.DateTime, default=datetime.now)
    updated_on = db.Column(
        db.DateTime, default=datetime.now, onupdate=datetime.now)
    cut_date = db.Column(db.DateTime)
    serial_number = db.Column(db.String(32), index=True)
    metal_type = db.Column(db.String(64))
    filetype = db.Column(db.String(6), index=True)
    filename = db.Column(db.String(64), index=True)
    filepath = db.Column(db.String(256), index=True)
    initials = db.Column(db.String(3), index=True)
    exp_unused_pct = db.Column(db.REAL())
    calc_unused_pct = db.Column(db.REAL())
    total_area = db.Column(db.REAL())
    used_area = db.Column(db.REAL())
    unused_area = db.Column(db.REAL())
    weight = db.Column(db.REAL())
    ratio = db.Column(db.REAL())
    ppi = db.Column(db.REAL())

    @staticmethod
    def cap_img_dir():
        return app.config['UPLOAD_FOLDER']

    @staticmethod
    def cap_dsp_dir():
        return os.path.join(app.config['STATIC_FOLDER'], 'images', 'tmp')

    @staticmethod
    def cap_cal_dir():
        return os.path.join(app.config['UPLOAD_FOLDER'], 'calibration')

    @staticmethod
    def get_calibration():
        calibration = None
        calibration_filepath = os.path.join(
            app.config['STATIC_FOLDER'], "calibrate.json")
        if os.path.exists(calibration_filepath):
            with open(calibration_filepath) as f:
                calibration = json.load(f)
        return calibration

    @staticmethod
    def fetch_image_sftp(dst):
        host = "126.10.240.20"
        port = 22
        un = "pi"
        pw = "r@spberry!"
        file = "/var/lib/motion/lastsnap.jpg"
        try:
            # fetch image
            transport = paramiko.Transport((host, port))
            transport.connect(username=un, password=pw, hostkey=None)
            sftp = paramiko.SFTPClient.from_transport(transport)
            sftp.get(file, dst)
            succ = True
        except Exception as e:
            print(e, flush=True)
            succ = False
        finally:
            sftp.close()
        return succ

    @staticmethod
    def fetch_image_avg(url, samples=3):
        print('fetch_image_avg: begin', flush=True)
        camera = cv2.VideoCapture(url)
        images = []
        for _ in range(samples):
            succ, frame = camera.read()
            if succ:
                images.append(frame)
        camera.release()
        print(
            f'fetch_image_avg: fetched ({len(images)}/{samples})',
            flush=True)
        if len(images) < int(samples / 2):
            print('fetch_image_avg: end NOK', flush=True)
            return False, None
        print('fetch_image_avg: end OK', flush=True)
        return True, ip.avg_images(images)

    @classmethod
    def expected_unused(cls, weight, metal_type):
        return (weight / cls.MTL_WEIGHT[metal_type]) * 100

    @classmethod
    def stage(cls):
        print('stage: begin', flush=True)
        # upload image file
        filename = f"{uuid4()}.jpg"
        img_path = os.path.join(cls.cap_img_dir(), filename)
        dsp_path = os.path.join(cls.cap_dsp_dir(), filename)
        cal_path = os.path.join(cls.cap_cal_dir(), filename)
        print(f'stage: img_path={img_path};', flush=True)
        print(f'stage: cal_path={cal_path};', flush=True)
        print(f'stage: dsp_path={dsp_path};', flush=True)
        try:
            print('stage: fetch image', flush=True)
            # succ = cls.fetch_image_sftp(img_path)
            url = f'http://{app.config["CAM_IP"]}:{app.config["CAM_PORT"]}/'
            succ, image = cls.fetch_image_avg(url, samples=3)
            print(f'stage: fetch image {succ}', flush=True)
            if not succ:
                raise Exception('Fetch image failed')
            cv2.imwrite(img_path, image)
            # create temp display image
            dsp_dir = cls.cap_dsp_dir()
            if (not os.path.exists(dsp_dir)):
                os.mkdir(dsp_dir)
            print('stage: copy to display path', flush=True)
            shutil.copy(img_path, dsp_path)
            # create calibration image
            cal_dir = cls.cap_cal_dir()
            if (not os.path.exists(cal_dir)):
                os.mkdir(cal_dir)
            print('stage: copy to calibration path', flush=True)
            shutil.copy(img_path, cal_path)
        except Exception as e:
            print(e, flush=True)
            print('stage: end NOK', flush=True)
            return False, None
        print('stage: end OK', flush=True)
        return succ, filename

    @classmethod
    def unstage(cls, filename, saved=False):
        print('unstage: begin', flush=True)
        # uploaded image files
        img_path = os.path.join(cls.cap_img_dir(), filename)
        cal_path = os.path.join(cls.cap_dsp_dir(), filename)
        dsp_path = os.path.join(cls.cap_cal_dir(), filename)
        # if we are not saving the image remove the uplaoded image
        if not saved and os.path.exists(img_path):
            print(f'unstage: removing "{img_path}"', flush=True)
            os.remove(img_path)
        # remove tmp files
        if os.path.exists(cal_path):
            print(f'unstage: removing "{cal_path}"', flush=True)
            os.remove(cal_path)
        if os.path.exists(dsp_path):
            print(f'unstage: removing "{dsp_path}"', flush=True)
            os.remove(dsp_path)
        print('unstage: end', flush=True)

    @classmethod
    def create(
        cls, filename, weight, metal_type, initials, cut_date, serial_number
    ):
        # get image file path
        filepath = os.path.join(cls.cap_img_dir(), filename)

        # get image
        image = cv2.imread(filepath)

        # undistort image
        calibration = cls.get_calibration()
        if calibration is not None:
            image = ip.undistort(image, calibration)
        else:
            print('calibration not found')

        # analyze image
        succ, vals = ip.analyze(image)
        if not succ:
            print('Analysis NOK')
            return False
        ttl_area, cut_area, side_lens = vals
        print(
            f'Analysis OK: ttl_area={ttl_area}, '
            f'cut_area={cut_area}, '
            f'side_lens={side_lens}')
        # calc ppi
        s_side, l_side = side_lens
        s_ppi = s_side / cls.SM_SIDE_IN
        l_ppi = l_side / cls.LG_SIDE_IN
        ratio = s_side / l_side
        a_ppi = (s_ppi + l_ppi) / 2
        a_area = (cls.SM_SIDE_IN * a_ppi) * (cls.LG_SIDE_IN * a_ppi) 
        print(f's_ppi={s_ppi}, l_ppi={l_ppi}, a_ppi={a_ppi}, ratio={ratio}')
        print(
            f'abs(a_area={a_area} - ttl_area={ttl_area}) = '
            f'{abs(a_area - ttl_area)}')

        # calc other
        cut_date = datetime.strptime(cut_date, '%Y-%m-%d')
        pathinfo = Path(filepath)
        filename = pathinfo.name
        filetype = pathinfo.suffix[1:]  # strip dot
        exp_unused_pct = cls.expected_unused(weight, metal_type)
        total_area = (s_side * l_side) / s_ppi ** 2
        used_area = cut_area / s_ppi ** 2
        unused_area = total_area - used_area
        calc_unused_pct = ((total_area - used_area) / total_area) * 100

        # create instance
        return cls(
            cut_date=cut_date,
            serial_number=serial_number,
            metal_type=metal_type,
            filetype=filetype,
            filename=filename,
            filepath=filepath,
            initials=initials,
            exp_unused_pct=exp_unused_pct,
            calc_unused_pct=calc_unused_pct,
            total_area=total_area,
            used_area=used_area,
            unused_area=unused_area,
            weight=weight,
            ratio=ratio,
            ppi=s_ppi,
        )

    def image_path(self):
        return url_for(
            'static', filename=f'images/metal/{self.id}.{self.filetype}')

    def image_path_sm(self):
        return url_for(
            'static', filename=f'images/metal/{self.id}_sm.{self.filetype}')

    def image_path_orig(self):
        return url_for(
            'static', filename=f'images/metal/{self.id}_orig.{self.filetype}')

    def __repr__(self):
        return f"<Sheet {self.id} {self.serial_number} {self.calc_unused_pct}>"

    def __iter__(self):
        yield "id", self.id
        yield "created_on", str(self.created_on)
        yield "updated_on", str(self.updated_on)
        yield "cut_date", str(self.cut_date)
        yield "serial_number", self.serial_number
        yield "metal_type", self.metal_type
        yield "filetype", self.filetype
        yield "filename", self.filename
        yield "filepath", self.filepath
        yield "initials", self.initials
        yield "exp_unused_pct", round(self.exp_unused_pct, 2)
        yield "calc_unused_pct", round(self.calc_unused_pct, 2)
        yield "total_area", round(self.total_area, 2)
        yield "used_area", round(self.used_area, 2)
        yield "unused_area", round(self.unused_area, 2)
        yield "weight", self.weight
        yield "ratio", round(self.ratio, 2)
        yield "ppi", round(self.ppi, 2)


# Event Hooks

@event.listens_for(Sheet, 'after_insert')
def receive_after_insert(mapper, connection, target):
    print('receive_after_insert: begin', flush=True)
    print('receive_after_insert: read image', flush=True)
    image = cv2.imread(target.filepath)
    print('receive_after_insert: generate debug image', flush=True)
    disp_image = ip.debug_image(image)
    # generate paths
    image_path = f'{BASE_DIR}{target.image_path()}'
    image_path_sm = f'{BASE_DIR}{target.image_path_sm()}'
    print(
        f'receive_after_insert: image_path={image_path}',
        f'\nreceive_after_insert: image_path_sm={image_path_sm}',
        flush=True)
    # write large display image
    print('receive_after_insert: write to image path', flush=True)
    cv2.imwrite(image_path, disp_image)
    # write small display image
    print('receive_after_insert: write to image path small', flush=True)
    cv2.imwrite(image_path_sm, ip.scale_image(disp_image, 0.1))
    # remove tmp images
    print('receive_after_insert: unstage', flush=True)
    Sheet.unstage(target.filename, saved=True)
    print('receive_after_insert: end', flush=True)


@event.listens_for(Sheet, 'after_delete')
def receive_after_delete(mapper, connection, target):
    print('receive_after_delete: begin', flush=True)
    # build image paths
    print('receive_after_delete: get image paths', flush=True)
    image_path = f'{BASE_DIR}{target.image_path()}'
    image_path_sm = f'{BASE_DIR}{target.image_path_sm()}'
    image_path_orig = target.filepath
    print(
        f'receive_after_delete: image_path={image_path}',
        f'\nreceive_after_delete: image_path_sm={image_path_sm}',
        f'\nreceive_after_delete: image_path_orig={image_path_orig}',
        flush=True)
    # remove original image if exists
    if os.path.exists(image_path_orig):
        print('receive_after_delete: removing image_file_orig', flush=True)
        os.remove(image_path_orig)
    else:
        print('receive_after_delete: image_file_orig not found', flush=True)
    # remove display image if exists
    if os.path.exists(image_path):
        print('receive_after_delete: removing image_file', flush=True)
        os.remove(image_path)
    else:
        print('receive_after_delete: image_file not found', flush=True)
    # remove small display image if exists
    if os.path.exists(image_path_sm):
        print('receive_after_delete: removing image_file_sm', flush=True)
        os.remove(image_path_sm)
    else:
        print('receive_after_delete: image_file_sm not found', flush=True)
    print('receive_after_delete: end', flush=True)