import cv2
import numpy as np
import math
from enum import Enum

weight_factors = {
    "alum_080": 34.90,
    "alum_063": 27.24,
}

class Mtl:
    MIN_PXL_AREA = 3300000


class Debug(Enum):
    CALIBRATION = 1
    SHEET_MASK  = 2
    METAL_MASK  = 3
    GET_CUTS    = 4


# Application Specific Functions

def get_cuts(image, debug=False):
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    # blur = cv2.GaussianBlur(hsv, (21, 21), 0)
    # hsv = cv2.addWeighted(hsv, 2, blur, -1, 0.0)
    # remove green sheet
    h_lo, s_lo, v_lo = 30, 90, 0
    h_hi, s_hi, v_hi = 80, 250, 240
    th = cv2.inRange(
        hsv, np.array([h_lo, s_lo, v_lo]), np.array([h_hi, s_hi, v_hi]))

    th = cv2.bitwise_not(th)
    open_kernel = np.ones((15, 15), np.uint8)
    th = cv2.morphologyEx(th, cv2.MORPH_CLOSE, open_kernel)
    th = cv2.bitwise_not(th)
    # remove noise
    # noise_rm_kernel = np.ones((5, 5), np.uint8)
    # th = cv2.erode(th, noise_rm_kernel, iterations=1)
    # th = cv2.dilate(th, noise_rm_kernel, iterations=1)
    # connect metal segments
    # open_kernel = np.ones((15, 15), np.uint8)
    # open_kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (11, 11))
    # open_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (11, 11))
    # th = cv2.morphologyEx(th, cv2.MORPH_OPEN, open_kernel)
    # th = cv2.bitwise_not(th)
    # kernel = np.ones((1, 20), np.uint8)
    # th = cv2.dilate(th, kernel)
    # th = cv2.erode(th, kernel)
    # kernel = np.ones((20, 1), np.uint8)
    # th = cv2.dilate(th, kernel)
    # th = cv2.erode(th, kernel)
    # th = cv2.bitwise_not(th)
    # debug sheet removal using saturation filter
    if debug:
        h, s, v = cv2.split(hsv)
        h_th = cv2.inRange(h, np.array([h_lo]), np.array([h_hi]))
        s_th = cv2.inRange(s, np.array([s_lo]), np.array([s_hi]))
        v_th = cv2.inRange(v, np.array([v_lo]), np.array([v_hi]))
        d_image = np.hstack((h, h_th))
        d_image = np.vstack((d_image, np.hstack((s, s_th))))
        d_image = np.vstack((d_image, np.hstack((v, v_th))))
        d_image = np.vstack((d_image, np.hstack((th, th))))
        # return th
        return d_image
    # find cut contours
    cut_cnts, _ = cv2.findContours(
        th, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    return cut_cnts


def not_sheet_mask(image, debug=False):
    image = image.copy()
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    hsv = cv2.bilateralFilter(hsv, 9, 75, 75)
    h_lo, s_lo, v_lo = 50, 115, 100
    h_hi, s_hi, v_hi = 80, 250, 255
    noise_rm_kernel = np.ones((5, 5), np.uint8)
    if debug:
        h, s, v = cv2.split(hsv)
        sheet_lo = np.array([h_lo])
        sheet_hi = np.array([h_hi])
        h_th = cv2.inRange(h, sheet_lo, sheet_hi)
        sheet_lo = np.array([s_lo])
        sheet_hi = np.array([s_hi])
        s_th = cv2.inRange(s, sheet_lo, sheet_hi)
        sheet_lo = np.array([v_lo])
        sheet_hi = np.array([v_hi])
        v_th = cv2.inRange(v, sheet_lo, sheet_hi)
        hsvstack = np.hstack((h, s))
        hsvstack = np.hstack((hsvstack, v))
        hsvth = np.hstack((h_th, s_th))
        hsvth = np.hstack((hsvth, v_th))
        th = np.vstack((hsvstack, hsvth))
        return th
    # mask sheet
    sheet_lo = np.array([h_lo, s_lo, v_lo])
    sheet_hi = np.array([h_hi, s_hi, v_hi])
    th = cv2.inRange(hsv, sheet_lo, sheet_hi)
    th = cv2.bitwise_not(th)
    # clean mask
    th = cv2.erode(th, noise_rm_kernel, iterations=1)
    th = cv2.dilate(th, noise_rm_kernel, iterations=1)
    return th


def sheet_mask(image, debug=False):
    scale_x = 0.99
    scale_y = 0.95
    image = image.copy()
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    h_lo, s_lo, v_lo = 50, 110, 100
    h_hi, s_hi, v_hi = 80, 255, 254
    noise_rm_kernel = np.ones((5, 5), np.uint8)
    close_kernel = np.ones((21, 21), np.uint8)
    if debug:
        h, s, v = cv2.split(hsv)
        sheet_lo = np.array([h_lo])
        sheet_hi = np.array([h_hi])
        h_th = cv2.inRange(h, sheet_lo, sheet_hi)
        sheet_lo = np.array([s_lo])
        sheet_hi = np.array([s_hi])
        s_th = cv2.inRange(s, sheet_lo, sheet_hi)
        sheet_lo = np.array([v_lo])
        sheet_hi = np.array([v_hi])
        v_th = cv2.inRange(v, sheet_lo, sheet_hi)
        hsvstack = np.hstack((h, s))
        hsvstack = np.hstack((hsvstack, v))
        hsvth = np.hstack((h_th, s_th))
        hsvth = np.hstack((hsvth, v_th))
        frame = np.vstack((hsvstack, hsvth))
        return frame
    # mask sheet
    sheet_lo = np.array([h_lo, s_lo, v_lo])
    sheet_hi = np.array([h_hi, s_hi, v_hi])
    th = cv2.inRange(hsv, sheet_lo, sheet_hi)
    # Clean Threshold
    th = cv2.erode(th, noise_rm_kernel, iterations=1)
    th = cv2.dilate(th, noise_rm_kernel, iterations=1)
    th = cv2.morphologyEx(th, cv2.MORPH_CLOSE, close_kernel)
    # get sheet contour
    cnts, _ = cv2.findContours(th, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    sheet_cnt = max(cnts, key=cv2.contourArea)
    hull = cv2.convexHull(sheet_cnt)
    hull = scale_contour(hull, (scale_x, scale_y))
    mask = np.zeros(th.shape[:2], np.uint8)
    mask = cv2.drawContours(mask, [hull], 0, [255], -1)
    return mask


def cnt_side_len(cnt):
    epsilon = 0.1 * cv2.arcLength(cnt, True)
    metal_corners = [
        (x, y) for [[x, y]] in
        cv2.approxPolyDP(cnt, epsilon, True)]
    if len(metal_corners) != 4:
        print(
            'Approx shape is not rectangle. len(metal_corners)=',
            len(metal_corners))
        return False, [None, None]
    point1, point2, point3, point4 = metal_corners
    side1 = (distance(point1, point2) + distance(point3, point4)) / 2
    side2 = (distance(point2, point3) + distance(point4, point1)) / 2
    return True, sorted([side1, side2])


def analyze(image):
    ERR_RETURN = False, None
    image = image.copy()
    # mask out area surrounding green sheet
    vig_rm_image = remove_vignette(image)
    mask = sheet_mask(vig_rm_image)
    sheet_mask_image = cv2.bitwise_and(vig_rm_image, vig_rm_image, mask=mask)
    # mask out area surrounding metal sheet
    metal_mask_image = not_sheet_mask(sheet_mask_image)
    metal_mask_image = cv2.bitwise_and(
        metal_mask_image, metal_mask_image, mask=mask)
    # get metal sheet contour
    cnts, _ = cv2.findContours(
        metal_mask_image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    metal_cnt = max(cnts, key=cv2.contourArea)
    metal_hull = cv2.convexHull(metal_cnt)
    # ensure metal sheet is found in image
    metal_area = cv2.contourArea(metal_hull)
    if metal_area < Mtl.MIN_PXL_AREA:
        return ERR_RETURN
    # mask out all but metal sheet
    metal_mask = np.zeros(metal_mask_image.shape[:2], np.uint8)
    metal_mask = cv2.drawContours(metal_mask, [metal_hull], 0, [255], -1)
    image = cv2.bitwise_and(image, image, mask=metal_mask)
    # find cuts
    cut_cnts = get_cuts(image)
    ttl_used_area = sum([cv2.contourArea(c) for c in cut_cnts])
    # get metal side len
    succ, side_lens = cnt_side_len(metal_cnt)
    if not succ:
        print('side len failure', flush=True)
        return ERR_RETURN
    # get pixels per inch
    return (True, (metal_area, ttl_used_area, side_lens))


# Image Formatting Functions

def chan_stack(image):
    chan1, *rest = cv2.split(image)
    s_image = chan1
    for c in rest:
        s_image = np.hstack((s_image, c))
    return s_image


def debug_image(image, debug=None):
    orig_image = image.copy()
    vig_rm_image = remove_vignette(image)
    # mask out area surrounding green sheet
    if debug == Debug.SHEET_MASK:
        mask = sheet_mask(vig_rm_image, True)
        return mask
    mask = sheet_mask(vig_rm_image)
    sheet_mask_image = cv2.bitwise_and(vig_rm_image, vig_rm_image, mask=mask)
    # mask out area surrounding metal sheet
    if debug == Debug.METAL_MASK:
        metal_mask_image = not_sheet_mask(sheet_mask_image, True)
        return metal_mask_image
    metal_mask_image = not_sheet_mask(sheet_mask_image)
    metal_mask_image = cv2.bitwise_and(
        metal_mask_image, metal_mask_image, mask=mask)
    # get metal sheet contour
    cnts, _ = cv2.findContours(
        metal_mask_image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    metal_cnt = max(cnts, key=cv2.contourArea)
    metal_hull = cv2.convexHull(metal_cnt)
    metal_area = cv2.contourArea(metal_hull)
    if metal_area >= Mtl.MIN_PXL_AREA:
        metal_mask = np.zeros(metal_mask_image.shape[:2], np.uint8)
        metal_mask = cv2.drawContours(metal_mask, [metal_hull], 0, [255], -1)
        image = cv2.bitwise_and(image, image, mask=metal_mask)
        # find cuts
        if debug == Debug.GET_CUTS:
            cuts = get_cuts(image, True)
            return cuts
        cuts = get_cuts(image)
        image = cv2.drawContours(orig_image, cuts, -1, (255, 0, 0), 5)
        image = cv2.drawContours(orig_image, [metal_hull], -1, (0, 0, 255), 5)
    else:
        # metal sheet is not centered display debug text
        image = cv2.putText(
            orig_image, 'Metal Sheet Not Centered', (50, 150),
            cv2.FONT_HERSHEY_SIMPLEX, 5, (0, 0, 255), 10, cv2.LINE_AA)
    return image


# Util Functions

def scale_image(image, scale):
    return cv2.resize(
        image, (int(image.shape[1] * scale), int(image.shape[0] * scale)))


def scale_contour(cnt, scale):
    scale_x, scale_y = scale
    M = cv2.moments(cnt)
    cx = int(M['m10']/M['m00'])
    cy = int(M['m01']/M['m00'])
    cnt_norm = cnt - [cx, cy]
    cnt_scaled = cnt_norm * [scale_x, scale_y]
    cnt_scaled = cnt_scaled + [cx, cy]
    cnt_scaled = cnt_scaled.astype(np.int32)
    return cnt_scaled


def avg_images(images):
    return np.mean(images, axis=0).astype(np.uint8)


def apply_contrast(image, contrast):
    image = image.copy()
    f = 131*(contrast + 127)/(127*(131-contrast))
    alpha_c = f
    gamma_c = 127*(1-f)
    image = cv2.addWeighted(image, alpha_c, image, 0, gamma_c)
    return image


def remove_vignette(image):
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    hsv = np.array(hsv, dtype=np.float64)
    # scale pixel values up or down for channel 1(Lightness)
    hsv[:, :, 1] = hsv[:, :, 1] * 1.3
    hsv[:, :, 1][hsv[:, :, 1] > 255] = 255
    # scale pixel values up or down for channel 1(Lightness)
    hsv[:, :, 2] = hsv[:, :, 2] * 1.3
    hsv[:, :, 2][hsv[:, :, 2] > 255] = 255
    hsv = np.array(hsv, dtype=np.uint8)
    return cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)
    # img = image
    # rows,cols = img.shape[:2]
    # zeros = np.copy(img)
    # zeros[:,:,:] = 0
    # a = cv2.getGaussianKernel(cols, 1600)
    # b = cv2.getGaussianKernel(rows, 1600)
    # c = b*a.T
    # d = c/c.max()
    # zeros = zeros.astype(np.int32)
    # zeros[:,:,0] = img[:,:,0] / d
    # zeros[:,:,1] = img[:,:,1] / d
    # zeros[:,:,2] = img[:,:,2] / d
    # zeros = np.clip(zeros, 0, 255)
    # vig_rm_image = zeros.astype(np.uint8)


def norm_illum(image):
    lab = cv2.cvtColor(image, cv2.COLOR_BGR2LAB)
    l, a, b = cv2.split(lab)
    clahe = cv2.createCLAHE(clipLimit=3.0, tileGridSize=(8, 8))
    cl = clahe.apply(l)
    limg = cv2.merge((cl, a, b))
    final = cv2.cvtColor(limg, cv2.COLOR_LAB2BGR)
    return final


def get_otsu(image, mask=None):
    hist = cv2.calcHist([image], [0], mask, [256], [0, 256])
    hist_norm = hist.ravel()/hist.sum()
    Q = hist_norm.cumsum()
    bins = np.arange(256)
    fn_min = np.inf
    thresh = -1
    for i in range(1, 256):
        p1, p2 = np.hsplit(hist_norm, [i])  # probabilities
        q1, q2 = (Q[i], Q[255] - Q[i])  # cum sum of classes
        if q1 < 1.e-6 or q2 < 1.e-6:
            continue
        b1, b2 = np.hsplit(bins, [i])  # weights
        # finding means and variances
        m1, m2 = (
            np.sum(p1 * b1) / q1,
            np.sum(p2 * b2) / q2)
        v1, v2 = (
            np.sum(((b1 - m1) ** 2) * p1) / q1,
            np.sum(((b2 - m2) ** 2) * p2) / q2)
        # calculates the minimization function
        fn = v1 * q1 + v2 * q2
        if fn < fn_min:
            fn_min = fn
            thresh = i
    return thresh


def horizontal_lines(image):
    image = image.copy()
    cols = image.shape[1]
    horizontal_size = cols // 30
    h_structure = cv2.getStructuringElement(
        cv2.MORPH_RECT, (horizontal_size, 1))
    image = cv2.erode(image, h_structure)
    image = cv2.dilate(image, h_structure)
    return image


def vertical_lines(image):
    image = image.copy()
    rows = image.shape[0]
    verticalsize = rows // 30
    v_structure = cv2.getStructuringElement(
        cv2.MORPH_RECT, (1, verticalsize))
    image = cv2.erode(image, v_structure)
    image = cv2.dilate(image, v_structure)
    return image


def undistort(img, calibration):
    dimensions = calibration["dimensions"]
    mtx = np.array(calibration["mtx"])
    dist = np.array(calibration["dist"])
    c_mtrx = np.array(calibration["c_mtrx"])
    [x, y, w, h] = calibration["roi"]
    # Uncomment for undistort transform
    # undistorted = cv2.undistort(img, mtx, dist, None, c_mtrx)
    # Uncomment for remapping transform
    mapx, mapy = cv2.initUndistortRectifyMap(
        mtx, dist, np.eye(3), c_mtrx, (dimensions[0], dimensions[1]), 0)
    undistorted = cv2.remap(
        img, mapx, mapy, interpolation=cv2.INTER_LINEAR,
        borderMode=cv2.BORDER_CONSTANT)
    # crop the image given the calibration roi, removing
    # any black pixels created by the undistortion.
    # Uncomment if undistort returns 500 or retrieved
    # images contain large black border.
    # undistorted = undistorted[y:y + int(h / 1.45), x:x + int(w / 1.45)]
    undistorted = undistorted[y:y + h, x:x + w]
    return undistorted


def distance(point1, point2):
    x1, y1 = point1
    x2, y2 = point2
    return math.hypot(x2 - x1, y2 - y1)