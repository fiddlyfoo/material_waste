import cv2
import numpy as np

# termination criteria
criteria = (
    cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER,
    30,
    0.1,
)

calibrate_flags = cv2.fisheye.CALIB_RECOMPUTE_EXTRINSIC\
    + cv2.fisheye.CALIB_FIX_SKEW

objp = np.zeros((4 * 4, 3), np.float32)
objp[:, :2] = np.mgrid[0:4, 0:4].T.reshape(-1, 2)

# Lists to store object points and image points from all the images.
objpoints = []  # 3d point in real world space
imgpoints = []  # 2d points in image plane.

h = 2448
w = 3264


def calibrate(images_list):
    """
    Given a list of image names, locate the checkerboard
    pattern in each image and record the object skew. Then
    calibrate the lens fisheye distortion based on the
    recorded skews.
    """
    global objpoints, imgpoints

    # iterate over all the images in the calibrations
    # directory.
    for fname in images_list:
        print(f"Evaluating {fname}...")
        # read the file into opencv
        img = cv2.imread(f"images/calibration/{fname}")

        # convert the loaded image to grayscale
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        # Find the chess board corners
        ret, corners = cv2.findChessboardCorners(
            gray,
            (4, 4),
            cv2.CALIB_CB_ADAPTIVE_THRESH
            + cv2.CALIB_CB_FAST_CHECK
            + cv2.CALIB_CB_NORMALIZE_IMAGE
        )

        # If found, add object points, image points (after refining them)
        if ret is True:
            objpoints.append(objp)

            corners2 = cv2.cornerSubPix(
                gray,
                corners,
                (3, 3),
                (-1, -1),
                criteria
            )

            imgpoints.append(corners2)

            # Uncomment to draw and display the corners.
            # img = cv2.drawChessboardCorners(img, (7, 6), corners2, ret)
            # cv2.imshow("img", img)
            # cv2.waitKey(1000)
        else:
            print("Unable to get checkerboard corners for this file.")

    ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(
        objpoints,
        imgpoints,
        (w, h),
        None,
        None,
        flags=calibrate_flags
    )

    c_mtrx, roi = cv2.getOptimalNewCameraMatrix(mtx, dist, (w, h), 1, (w, h))

    # Uncomment if cv2.imshow is used.
    # cv2.destroyAllWindows()

    return {
        "dimensions": (w, h),
        "mtx": mtx.tolist(),
        "dist": dist.tolist(),
        "c_mtrx": c_mtrx.tolist(),
        "roi": roi,
    }
