from numpy.core.function_base import linspace
import pandas
import os
import shutil
import io
import math

import cv2
import numpy as np

import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from pprint import pprint
from flask import (
    after_this_request, Response,
    abort, flash, json, jsonify,
    render_template, request, send_file,
    Blueprint, current_app as app
)
from sqlalchemy import func
from functools import reduce
from datetime import datetime, timedelta
from app import db
from app.models import Sheet
import app.image_proc as ip


bp = Blueprint("main", __name__)


# TODO: should have all file creation and handled in Sheet Model

def get_corners(cnt):
    # get bounding rect corners
    min_rect = cv2.minAreaRect(cnt)
    box = cv2.boxPoints(min_rect)
    pts = box.reshape(4, 2).tolist()
    pts = sorted(pts, key=lambda p: p[1])
    tl_pt, tr_pt = sorted(pts[:2], key=lambda p: p[0])
    bl_pt, br_pt = sorted(pts[-2:], key=lambda p: p[0])
    # print('min area rect points')
    # print('tl_pt', tl_pt, flush=True)
    # print('tr_pt', tr_pt, flush=True)
    # print('bl_pt', bl_pt, flush=True)
    # print('br_pt', br_pt, flush=True)
    c_tl_pt = min(cnt, key=lambda p: ip.distance(p[0], tl_pt))[0]
    c_tr_pt = min(cnt, key=lambda p: ip.distance(p[0], tr_pt))[0]
    c_br_pt = min(cnt, key=lambda p: ip.distance(p[0], br_pt))[0]
    c_bl_pt = min(cnt, key=lambda p: ip.distance(p[0], bl_pt))[0]
    # print('hull points')
    # print('c_tl_pt', c_tl_pt, flush=True)
    # print('c_tr_pt', c_tr_pt, flush=True)
    # print('c_bl_pt', c_bl_pt, flush=True)
    # print('c_br_pt', c_br_pt, flush=True)
    return [c_tl_pt, c_tr_pt, c_br_pt, c_bl_pt]


def ud(image):
    width = image.shape[1]
    height = image.shape[0]
    # k1 = -1.5e-6  # negative to remove barrel distortion
    k1 = -1.5e-6  # negative to remove barrel distortion
    k2 = 0.0
    p1 = 0.0
    p2 = 0.0
    k3 = 0.0
    dist = np.array([
        [k1, k2, p1, p2, k3]
    ])
    # assume unit matrix for camera
    # focal_x = 10.0              # define focal length x
    # focal_y = 10.0              # define focal length y
    focal_x = 10.0              # define focal length x
    focal_y = 10.0              # define focal length y
    center_y = (height / 2.0)     # define center y
    center_x = (width / 2.0)      # define center x
    mtx = np.array([
        [focal_x, 0.0, center_x],
        [0.0, focal_y, center_y],
        [0.0, 0.0, 1.0]
    ])
    dst = cv2.undistort(image, mtx, dist)
    # center = (dst.shape[1] // 2, dst.shape[0] // 2)
    # angle = -50
    # scale = 0.6
    # rot_mat = cv2.getRotationMatrix2D(center, angle, scale)
    # dst = cv2.warpAffine(dst, rot_mat, (dst.shape[1], dst.shape[0]))
    return dst


def pt(image, cnt):
    min_rect = cv2.minAreaRect(cnt)
    box = cv2.boxPoints(min_rect)
    pts = box.reshape(4, 2)
    rect = np.zeros((4, 2), dtype=np.float32)
    # the top-left point has the smallest sum whereas the
    # bottom-right has the largest sum
    s = pts.sum(axis=1)
    rect[0] = pts[np.argmin(s)]
    rect[2] = pts[np.argmax(s)]
    # compute the difference between the points -- the top-right
    # will have the minumum difference and the bottom-left will
    # have the maximum difference
    diff = np.diff(pts, axis=1)
    rect[1] = pts[np.argmin(diff)]
    rect[3] = pts[np.argmax(diff)]
    # # multiply the rectangle by the original ratio
    # rect *= ratio
    # now that we have our rectangle of points, let's compute
    # the width of our new image
    (tl, tr, br, bl) = rect
    widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
    widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
    # ...and now for the height of our new image
    heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
    heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
    # take the maximum of the width and height values to reach
    # our final dimensions
    maxWidth = max(int(widthA), int(widthB))
    maxHeight = max(int(heightA), int(heightB))
    # construct our destination points which will be used to
    # map the screen to a top-down, "birds eye" view
    dst = np.array([
        [0, 0],
        [maxWidth - 1, 0],
        [maxWidth - 1, maxHeight - 1],
        [0, maxHeight - 1]], dtype=np.float32)
    # calculate the perspective transform matrix and warp
    # the perspective to grab the screen
    M = cv2.getPerspectiveTransform(rect, dst)
    warp = cv2.warpPerspective(image, M, (maxWidth, maxHeight))
    return warp


def other(image, corners):
    from scipy.interpolate import griddata
    n_image_w = int(ip.distance(corners[0], corners[1]))
    n_image_h = int(ip.distance(corners[1], corners[2]))
    grid_x, grid_y = np.mgrid[
        0:n_image_w-1:np.complex(0, n_image_w),
        0:n_image_h-1:np.complex(0, n_image_h)]
    source = np.array([corners[0], corners[3], corners[1], corners[2], ])
    print(source, flush=True)
    destination = np.array([
        [0, 0], [0, n_image_h-1],
        [n_image_w-1, 0], [n_image_w-1, n_image_h-1] ])
    grid_z = griddata(destination, source, (grid_x, grid_y), method='cubic')
    # grid_z = griddata(destination, source, (grid_y, grid_x), method='cubic')
    map_x = np.append([], [ar[:, 1] for ar in grid_z])\
        .reshape(n_image_w, n_image_h)
    map_y = np.append([], [ar[:, 0] for ar in grid_z])\
        .reshape(n_image_w, n_image_h)
    map_x_32 = map_x.astype('float32')
    map_y_32 = map_y.astype('float32')
    image = cv2.remap(image, map_x_32, map_y_32, cv2.INTER_CUBIC)


def segment_contour_sides(image, cnt):
    corners = get_corners(cnt)
    print('corners', corners, flush=True)
    for x, y in corners:
        image = cv2.circle(image, (int(x), int(y)), 1, (0, 0, 255), 10)
    cnt_indexes = [None] * len(corners)
    for i, [[x, y]] in enumerate(cnt):
        for j, (cx, cy) in enumerate(corners):
            if cx == x and cy == y:
                cnt_indexes[j] = i
    cnt_indexes = sorted(cnt_indexes)
    left = cnt[cnt_indexes[0]:cnt_indexes[1]]
    bottom = cnt[cnt_indexes[1]:cnt_indexes[2]]
    right = cnt[cnt_indexes[2]:cnt_indexes[3]]
    top = np.concatenate((
        np.flip(cnt[cnt_indexes[3]:], axis=0),
        cnt[:cnt_indexes[0]]))
    left_color = (255, 0, 0)
    bottom_color = (0, 255, 0)
    right_color = (0, 0, 255)
    top_color = (255, 255, 0)
    for [[x, y]] in left:
        image = cv2.circle(image, (int(x), int(y)), 1, left_color, 10)
    for [[x, y]] in bottom:
        image = cv2.circle(image, (int(x), int(y)), 1, bottom_color, 10)
    for [[x, y]] in right:
        image = cv2.circle(image, (int(x), int(y)), 1, right_color, 10)
    for [[x, y]] in top:
        image = cv2.circle(image, (int(x), int(y)), 1, top_color, 10)
    tlc, trc, brc, blc = corners


@bp.route("/sheets/<int:sheet_id>/debug", methods=["GET"])
def sheet_debug(sheet_id):
    sheet = Sheet.query.get(sheet_id) or abort(404)
    image = cv2.imread(sheet.filepath)

    image = ud(image)

    vig_rm_image = ip.remove_vignette(image)
    mask = ip.sheet_mask(vig_rm_image)
    sheet_mask_image = cv2.bitwise_and(vig_rm_image, vig_rm_image, mask=mask)
    # mask out area surrounding metal sheet
    metal_mask_image = ip.not_sheet_mask(sheet_mask_image)
    metal_mask_image = cv2.bitwise_and(
        metal_mask_image, metal_mask_image, mask=mask)
    # get metal sheet contour
    cnts, _ = cv2.findContours(
        metal_mask_image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    metal_cnt = max(cnts, key=cv2.contourArea)
    metal_hull = cv2.convexHull(metal_cnt)

    # mask out all but metal sheet
    metal_mask = np.zeros(metal_mask_image.shape[:2], np.uint8)
    metal_mask = cv2.drawContours(metal_mask, [metal_hull], 0, [255], -1)
    image = cv2.bitwise_and(image, image, mask=metal_mask)

    # find cuts
    cuts = ip.get_cuts(image)
    metal_mask = cv2.drawContours(metal_mask, cuts, -1, [0], -1)
    image = cv2.bitwise_and(image, image, mask=metal_mask)

    metal_cnt = ip.scale_contour(metal_cnt, (0.99, 0.99))
    image = pt(image, metal_cnt)
    metal_mask = pt(metal_mask, metal_cnt)

    cut_masks = cv2.bitwise_not(metal_mask)
    cnts, _ = cv2.findContours(
        cut_masks, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    hulls = [cv2.convexHull(cnt) for cnt in cnts]
    image = cv2.drawContours(image, hulls, -1, [0, 0, 255], -1)


    succ, buffer = cv2.imencode('.jpg', ip.scale_image(image, 0.3))
    if not succ:
        abort(500)
    return Response(buffer.tobytes(), mimetype='image/jpeg')


@bp.route("/sheets/<int:sheet_id>/grid", methods=["GET"])
def sheet_grid(sheet_id):
    sheet = Sheet.query.get(sheet_id) or abort(404)
    image = cv2.imread(sheet.filepath)

    # calibration = Sheet.get_calibration()
    # if calibration is not None:
    #     image = ip.undistort(image, calibration)
    image = ud(image)

    vig_rm_image = ip.remove_vignette(image)
    mask = ip.sheet_mask(vig_rm_image)
    sheet_mask_image = cv2.bitwise_and(vig_rm_image, vig_rm_image, mask=mask)
    # mask out area surrounding metal sheet
    metal_mask_image = ip.not_sheet_mask(sheet_mask_image)
    metal_mask_image = cv2.bitwise_and(
        metal_mask_image, metal_mask_image, mask=mask)
    # get metal sheet contour
    cnts, _ = cv2.findContours(
        metal_mask_image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    metal_cnt = max(cnts, key=cv2.contourArea)
    metal_hull = cv2.convexHull(metal_cnt)

    # mask out all but metal sheet
    metal_mask = np.zeros(metal_mask_image.shape[:2], np.uint8)
    metal_mask = cv2.drawContours(metal_mask, [metal_hull], 0, [255], -1)
    image = cv2.bitwise_and(image, image, mask=metal_mask)

    # find cuts
    cuts = ip.get_cuts(image)
    metal_mask = cv2.drawContours(metal_mask, cuts, -1, [0], -1)
    image = cv2.bitwise_and(image, image, mask=metal_mask)

    metal_cnt = ip.scale_contour(metal_cnt, (0.99, 0.99))
    image = pt(image, metal_cnt)
    metal_mask = pt(metal_mask, metal_cnt)


    PXL_P_UNIT = 2
    w = 8 * 12 * 4 * PXL_P_UNIT
    h = 4 * 12 * 4 * PXL_P_UNIT
    BOX_W = PXL_P_UNIT
    BOX_H = PXL_P_UNIT
    # BOX_A = (BOX_W * BOX_H) * 0.5
    BOX_A = 0
    BDT = int(PXL_P_UNIT / 2)
    image = cv2.resize(image, (w, h))
    metal_mask = cv2.resize(metal_mask, (w, h))
    bound_h, bound_w = image.shape[:2]
    bound_x, bound_y = 0, 0
    rects = []
    for i in range(0, math.ceil(bound_h / BOX_H)):
        y = bound_y + (BOX_H * i)
        row = []
        for j in range(0, math.ceil(bound_w / BOX_W)):
            x = bound_x + (BOX_W * j)
            section = metal_mask[y:y + BOX_H, x:x + BOX_W]
            non_zero = cv2.countNonZero(section)
            if non_zero > BOX_A:
                start_point = (x, y)
                end_point = (x + BOX_W, y + BOX_H)
                image = cv2.rectangle(
                    image,
                    (start_point[0] + BDT, start_point[1] + BDT),
                    (end_point[0] - BDT, end_point[1] - BDT),
                    (255, 0, 0), BDT)
                row.append((True, (start_point, end_point)))
            else:
                start_point = (x, y)
                end_point = (x + BOX_W, y + BOX_H)
                image = cv2.rectangle(
                    image,
                    (start_point[0] + BDT, start_point[1] + BDT),
                    (end_point[0] - BDT, end_point[1] - BDT),
                    (0, 0, 255), BDT)
                row.append((False, (start_point, end_point)))
        rects.append(row)

    # succ, buffer = cv2.imencode('.jpg', ip.scale_image(image, 0.3))
    # if not succ:
    #     abort(500)
    # return Response(buffer.tobytes(), mimetype='image/jpeg')

    cut_masks = cv2.bitwise_not(metal_mask)
    cnts, _ = cv2.findContours(
        cut_masks, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    hulls = [[[int(x), int(y)] for [[x, y]] in cv2.convexHull(cnt)] for cnt in cnts]

    return render_template(
        'test.html',
        rects=json.dumps(rects),
        cuts=json.dumps(hulls))


@bp.route("/")
def index():
    """
    Returns a Jinja template that renders the index page/
    dashboard. Optionally, a date range can be supplied,
    showing only the stats for that range.
    """
    sheets = None

    # Extract start and end dates from query string.
    start_date = request.args.get('start_date')
    end_date = request.args.get('end_date')

    # if there is a date range filter, get only the
    # entries that were entered within the date range
    if start_date is not None and end_date is not None:
        sheets = Sheet.query.filter(
            Sheet.created_on.between(start_date, end_date)).all() or []
    # if there is a start date, but no end date
    elif start_date is not None and end_date is None:
        sheets = Sheet.query.filter(
            Sheet.created_on >= start_date
        ).all() or []
    # if there is no start date, but an end date
    elif start_date is None and end_date is not None:
        sheets = Sheet.query.filter(
            Sheet.created_on <= end_date
        ).all() or []
    else:
        sheets = Sheet.query.filter(
            Sheet.created_on.between(
                datetime.now().date() - timedelta(days=30),
                datetime.now().date(),
            )).all() or []

    unused_pcts = []
    total_count = 0
    average_unused_pct = 0
    unused_weights = []
    average_unused_weight = 0
    total_unused_weight = 0

    if len(sheets):
        unused_pcts = [round(s.exp_unused_pct, 2) for s in sheets]
        total_count = len(unused_pcts)
        unused_pcts = [p for p in unused_pcts if p > 0]

        # Get the average of all recorded sheets' unused
        # percentages
        average_unused_pct = reduce(
            lambda accum, val: accum + val, unused_pcts
        ) / total_count

        unused_weights = [round(s.weight, 2) for s in sheets]
        average_unused_weight = reduce(
            lambda accum, val: accum + val, unused_weights
        ) / total_count
        total_unused_weight = sum(unused_weights)

    return render_template(
        "index.html",
        average_unused_pct=average_unused_pct,
        average_unused_weight=average_unused_weight,
        total_count=total_count,
        total_unused_weight=total_unused_weight,
        unused_pcts=unused_pcts,
        unused_weights=unused_weights,
        start_date=start_date,
        end_date=end_date,
    )


@bp.route("/graph-example", methods=["GET"])
def graph_example():
    sheets = Sheet.query.limit(30).all()

    return render_template(
        "graph-example.html",
        sheets=[dict(sheet) for sheet in sheets],
    )


@bp.route("/graph-unused", methods=["GET"])
def graph_unused():
    sheets = db.session\
        .query(
            func.avg(Sheet.calc_unused_pct),
            func.sum(Sheet.weight),
            func.date_format(Sheet.created_on, "%Y-%m-%d"))\
        .filter(
            Sheet.created_on.between(
                datetime.today() - timedelta(days=30), datetime.now()))\
        .group_by(func.date_format(Sheet.created_on, "%Y-%m-%d"))\
        .all()

    avg_pct = [s[0] for s in sheets]
    weights = [s[1] for s in sheets]
    dates = [s[2] for s in sheets]

    data = []

    for i, date in enumerate(dates):
        data.append(
            [datetime.strptime(date, "%Y-%m-%d"), avg_pct[i], weights[i]])

    df = pandas.DataFrame(data, columns=["date", "avg_pct", "sum_weight"])
    df.set_index(df.date, inplace=True)
    df = df.resample("D").sum().fillna(0)

    fig, ax = plt.subplots(figsize=(8, 5))
    plot = df.plot(ax=ax)

    ax.xaxis.set_major_formatter(mdates.DateFormatter("%b %d"))
    buf = io.BytesIO()
    fig.savefig(buf, format="png")
    plt.clf()
    buf.seek(0)

    return send_file(
        buf,
        mimetype="image/png",
        as_attachment=False,
        attachment_filename="graph_unused.png"
    )


@bp.route("/docs", methods=["GET"])
def docs():
    """
    Returns the docs template
    """

    return render_template("docs.html")


@bp.route("/sheets", methods=["GET", "POST"])
def sheets():
    """
    GET request returns a Jinja template that renders an
    entry for each image in the immediate images directory
    (does not walk).

    POST request receives a single image (called "file")
    that loads into the main images directory and runs the
    script on the image provided.
    """

    # handle GET Request
    if request.method == "GET":
        # Extract start and end dates from query string.
        start_date = request.args.get("start_date")
        end_date = request.args.get("end_date")

        recorded_sheets = []

        # if there is a date range filter, get only the
        # entries that were entered within the date range
        if start_date is not None and end_date is not None:
            recorded_sheets = Sheet.query.filter(
                Sheet.created_on.between(start_date, end_date)
            ).all() or []

        # if there is a start date, but no end date
        elif start_date is not None and end_date is None:
            recorded_sheets = Sheet.query.filter(
                Sheet.created_on >= start_date
            ).all() or []

        # if there is no start date, but an end date
        elif start_date is None and end_date is not None:
            recorded_sheets = Sheet.query.filter(
                Sheet.created_on <= end_date
            ).all() or []

        # otherwise, get all entries from the database in the
        # last 30 days.
        else:
            filter_after = datetime.today() - timedelta(days=30)
            recorded_sheets = Sheet.query.filter(
                Sheet.created_on >= filter_after
            ).all() or []

        # return the template with an array of sheet
        # entries.
        return render_template(
            "sheets.html",
            sheets=recorded_sheets,
            start_date=start_date,
            end_date=end_date,
        )
    # handle POST Request
    elif request.method == "POST":
        # Get cut date, serial no., weight, initials, and
        # file from form
        f = request.files["file"]

        cut_date = request.form.get("cut_date")
        serial_number = request.form.get("serial_number")
        weight = float(request.form.get("weight"))
        initials = request.form.get("initials")
        metal_type = request.form.get("metal_type")

        # convert cut_date to datetime object
        cut_date = datetime.strptime(cut_date, '%Y-%m-%d')

        # if there is no filename, flash that on the
        # frontend (not currently implemented.)
        if f.filename == "":
            flash("No selected file.")

        # extract the filename from the extension for
        # logging purposes.
        fname, ftype = f.filename.split(".")

        # if there is a file and the extension is allowed,
        # rewrite the filename as secure and write the file
        # to the images/ directory at project root.
        if f and allowed_file(f.filename):
            filename = secure_filename(f"{serial_number}.{ftype}")
            f.save(os.path.join(app.config["UPLOAD_FOLDER"], filename))
        else:
            abort(415)  # 415 - Unsupported Media Type

        # run the analysis on the picture.
        print(filename)
        analysis = ip.analyze(
            filename,
            weight,
            metal_type,
            initials, 
            app.config['UPLOAD_FOLDER'],
            app.config['STATIC_FOLDER']
        )

        # create a new sheet from analysis and dialog info.
        s = Sheet(
            cut_date=cut_date,
            serial_number=serial_number,
            metal_type=metal_type,
            filetype=ftype,
            filename=fname,
            filepath=f"{app.config['UPLOAD_FOLDER']}/originals/{filename}",
            initials=initials,
            exp_unused_pct=analysis["expected_unused_pct"],
            calc_unused_pct=analysis["calculated_unused_pct"],
            total_area=analysis["total_area"],
            used_area=analysis["used_area"],
            unused_area=analysis["unused_area"],
            weight=analysis["weight"],
            ratio=analysis["ratio"],
            ppi=analysis["ppi"],
        )

        # add the new sheet to the database and commit the
        # entry.
        db.session.add(s)
        db.session.commit()

        # pretty print the sheet for logging purposes.
        pprint(dict(s))

        @after_this_request
        def add_headers(r):
            r.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
            r.headers["Pragma"] = "no-cache"
            r.headers["Expires"] = "0"
            r.headers['Cache-Control'] = 'public, max-age=0'
            return r

        # Flask has an interesting time being both unable
        # to send a dict object as a response, and unable
        # to serialize an instance of the Sheet class. To
        # get around this, we can cast the sheet instance
        # to a dict, and then serialize the dict.
        return jsonify(dict(s))


@bp.route("/sheets/<int:sheet_id>", methods=["GET", "DELETE"])
def sheet(sheet_id):
    """
    GET Returns a template, providing only the sheet name.

    DELETE deletes a template by its ID and should return
    the user to the /sheets page. Delete requests should
    always return a 204 status code with an empty response.
    """
    sheet = Sheet.query.get(sheet_id) or abort(404)

    if request.method == "GET":
        return render_template("sheet.html", sheet=sheet)
    elif request.method == "DELETE":
        image_path = os.path.join(
            app.config['UPLOAD_FOLDER'], 'retrieved', sheet.serial_number)
        web_image = os.path.join(
            app.config['STATIC_FOLDER'], 'images',
            f"{sheet.serial_number}.{sheet.filetype}")

        # delete the picture contents
        if os.path.exists(image_path) and os.path.isdir(image_path):
            shutil.rmtree(image_path)

        # delete the web picture contents
        if os.path.exists(web_image) and os.path.isfile(web_image):
            os.remove(web_image)

        # Delete the row from the database
        db.session.delete(sheet)
        db.session.commit()

        @after_this_request
        def add_headers(r):
            r.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
            r.headers["Pragma"] = "no-cache"
            r.headers["Expires"] = "0"
            r.headers['Cache-Control'] = 'public, max-age=0'
            return r

        # Return a 204 response (No Content) as there won't
        # be content since we deleted it.
        return ("", 204)


@bp.route("/camera", methods=["GET"])
def camera_page():
    """
    Renders the camera.html template
    """
    return render_template("camera.html")


@bp.route("/capture", methods=["GET"])
def capture_image():
    """
    GET captures an image from the live feed by calling the
    /{cameraId}/action/snapshot and waits for a 200 OK
    status code. Once received, an SFTP connection is
    opened to retrieve the newly created file (as Motion
    does NOT send the image back as a Response). Once the
    image is transferred, it is renamed using UUID v4 and
    the {UUID}.png is returned.
    """
    # calibrate_mode = request.args.get("calibrate")
    success, filename = Sheet.stage()
    if not success:
        abort(500)

    @after_this_request
    def add_headers(r):
        r.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
        r.headers["Pragma"] = "no-cache"
        r.headers["Expires"] = "0"
        r.headers['Cache-Control'] = 'public, max-age=0'
        return r

    # return the filename to the Client (browser) to bring
    # up the dialog.
    return jsonify(dict({"filename": filename}))


@bp.route("/record", methods=["POST"])
def record_capture():
    """
    Given a known filename created by the Flask app and
    one's initials and sheet weight, run an analysis on the
    image with the given filename.
    """
    # Get the filename, initials, and weight from the
    # formdata
    filename = request.form.get("filename")
    serial_number = request.form.get("serial_number")
    initials = request.form.get("initials")
    weight = float(request.form.get("weight"))
    metal_type = request.form.get("metal_type")
    cut_date = (
        request.form.get("cut_date")
        or datetime.now().strftime('%Y-%m-%d'))

    # check if a record already exists with the requested
    # if it does, abort with a 409 Conflict status code.
    exists = Sheet.query.\
        filter_by(serial_number=serial_number)\
        .first()
    if exists:
        abort(409)  # 409 - Conflict

    # generate sheet record
    sheet = Sheet.create(
        filename,
        weight,
        metal_type,
        initials,
        cut_date,
        serial_number
    )
    db.session.add(sheet)
    db.session.commit()

    return jsonify(dict(sheet))


@bp.route("/discard", methods=["POST"])
def discard_capture():
    """
    POST takes a filename in the form of FormData and
    removes the file from `static/images/tmp` and `images/`.
    This function is used exclusively on the Analysis
    dialog in the event that the user wants to re-capture
    a picture. The idea is that the original image
    shouldn't be kept on the server.
    """
    filename = request.form.get("filename") or request.args.get("filename")
    Sheet.unstage(filename)
    # Return a 204 response (No Content) as there won't
    # be content since we deleted it.
    return ("", 204)


# TODO: look at how cailbration is handled
@bp.route("/calibrate", methods=["GET"])
def handle_calibrate():
    """
    Calibrate consumes all images in images/calibration and
    attempts to calibrate the camera lens based on the
    provided images. Returns a 400 Bad Request if fewer
    than 10 images exist in the folder. Returns a 200 OK
    when the calibration finishes.
    """
    p = "images/calibration"
    print("Reading images for calibration...")
    calibrate_images = [
        f for f in os.listdir(p)
        if os.path.isfile(os.path.join(p, f))
    ]

    if len(calibrate_images) < 10:
        print("Fewer than 10 images provided!")
        abort(400)

    print("Calibrating...")
    result = ip.calibrate(calibrate_images)

    print("Saving result...")
    cal_file = os.path.join(app.config['STATIC_FOLDER'], "calibrate.json")
    with open(cal_file, "w") as f:
        json.dump(result, f, indent=2)

    print("Moving image set to folder...")
    now = datetime.now()
    dir_name_date = now.strftime("%Y%m%d%H%M%S")
    os.mkdir(os.path.join(
        app.config['UPLOAD_FOLDER'], 'calibration', dir_name_date))

    for image in calibrate_images:
        shutil.move(
            os.path.join(
                app.config['UPLOAD_FOLDER'], 'calibration', image),
            os.path.join(
                app.config['UPLOAD_FOLDER'], 'calibration', dir_name_date,
                image)
        )
    return jsonify(result)


@bp.route("/report", methods=["GET"])
def generate_report():
    """
    Generate Report for either a day, week, or month, as
    well as proper constraint. For example:

        group_by='d'
        value='2020-07-03'

    If using `group_by='d'` without a value parameter, we
    will make the assumption that we are querying for the
    current date. The same can be said about the 'w' and 'm'
    parameters. If no value is supplied, query for the
    current week or current month.
    """
    group_by = request.args.get("group_by", 'm')

    start_date = datetime.strptime(request.args.get('start_date')).date() \
        if request.args.get('start_date') is not None else \
        (datetime.now() - timedelta(days=30)).date()

    end_date = datetime.strptime(request.args.get('end_date')).date() \
        if request.args.get('end_date') is not None else \
        datetime.now().date()

    formatter = ""

    if group_by == "d":
        formatter = "%Y-%m-%d"
    elif group_by == "m":
        formatter = "%Y-%m"

    qb = db.session.query(
        func.count(Sheet.id),
        func.avg(Sheet.exp_unused_pct),
        func.avg(Sheet.calc_unused_pct),
        func.sum(Sheet.weight),
        func.date_format(Sheet.created_on, formatter),
    ).filter(
        Sheet.created_on.between(start_date, end_date)
    )

    if group_by == "d":
        qb = qb.group_by(
            func.date_format(Sheet.created_on, formatter)
        )
    elif group_by == "m":
        qb = qb.group_by(
            func.date_format(Sheet.created_on, formatter)
        )

    sheets = [{
        "total_sheets": s[0],
        "avg_exp_unused_pct": round(s[1], 2),
        "avg_calc_unused_pct": round(s[2], 2),
        "total_weight": round(s[3], 2),
        "date": s[4],
    } for s in qb.all()]

    return jsonify(sheets)
