from app.routes.main import bp as main_bp
from app.routes.kv import bp as kv_bp
from app.routes.image import bp as image_bp


def init_app(app):
    app.register_blueprint(main_bp)
    app.register_blueprint(kv_bp)
    app.register_blueprint(image_bp)
    return app
