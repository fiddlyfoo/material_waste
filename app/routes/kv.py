from flask import abort, jsonify, Blueprint, current_app as app
from os import getenv
from datetime import datetime
from sqlalchemy import func
from pprint import pprint

from app import db
from app.models import Sheet
from app.image_proc import weight_factors


bp = Blueprint("kivy", __name__)


@bp.route("/kivy/camera", methods=["GET"])
def kivy_camera_page():
    """
    Returns a KV screen to a Kivy application
    """
    return jsonify({
        "contents": f"""
Screen:
    name: 'metal_waste_camera'
    BoxLayout:
        padding: '20dp'
        spacing: '10sp'
        orientation: 'vertical'
        MDRaisedButton:
            text: 'Dashboard'
            on_release: app.fetch(f'http://{getenv('MY_IP')}:9034/kivy/dashboard', on_success=lambda _req, _res: app.screen_manager.replace_screen(_res["contents"]))
        MDRaisedButton:
            text: 'All Sheets'
            on_release: app.fetch(f'http://{getenv('MY_IP')}:9034/kivy/results/last', on_success=lambda _req, _res: app.screen_manager.replace_screen(_res["contents"]))
        RefreshImage:
            id: wv
            interval: 5
            source: 'http://{getenv('MY_IP')}:9034/image/still/small'
        MDRaisedButton:
            text: 'Capture & Log'
            pos_hint: {{"center_x": .5, "center_y": .5}}
            on_release: app.fetch(\
            'http://{getenv('MY_IP')}:9034/capture', \
            on_success=lambda request, response: app.fetch(f'http://{getenv('MY_IP')}:9034/kivy/record/{{response["filename"]}}', \
            on_success=lambda request, response: app._popup_open(response["contents"])) \
            )
""",
        "name": "metal_waste_camera"
    })


@bp.route("/kivy/dashboard", methods=["GET"])
def kivy_dashboard():
    """
    Returns a KV screen to a Kivy application
    """
    today_date = bytes(datetime.now().strftime("%Y-%m-%d"), "utf-8")

    sheets = db.session.query(
        func.count(Sheet.id),
        func.avg(Sheet.calc_unused_pct),
        func.sum(Sheet.weight),
    ).filter(
        func.substr(
            Sheet.created_on,
            1, 10
        ) == today_date
    ).one()

    count = sheets[0] or 0
    avg_pct = sheets[1] or 0
    weights = sheets[2] or 0

    return jsonify({
        "contents": f"""
Screen:
    name: 'metal_waste_dashboard'
    on_enter: unused_sheet_graph.reload()
    BoxLayout:
        padding: '20dp'
        spacing: '10sp'
        orientation: 'vertical'
        MDLabel:
            text: 'Today Summary'
            halign: 'center'
            valign: 'middle'
            font_size: '36sp'
            size_hint: 1, None
        GridLayout:
            cols: 2
            row_force_default: True
            row_default_height: 150
            BoxLayout:
                orientation: 'vertical'
                MDLabel:
                    text: '# of Sheets'
                    halign: 'center'
                    valign: 'middle'
                MDLabel:
                    text: '{count}'
                    halign: 'center'
                    valign: 'middle'
                    font_size: '36sp'
            BoxLayout:
                orientation: 'vertical'
                MDLabel:
                    text: 'Unused Weight'
                    halign: 'center'
                    valign: 'middle'
                MDLabel:
                    text: '{weights:.2f} lbs'
                    halign: 'center'
                    valign: 'middle'
                    font_size: '36sp'
            BoxLayout:
                orientation: 'vertical'
                MDLabel:
                    text: 'Average Sheet Unused'
                    halign: 'center'
                    valign: 'middle'
                MDLabel:
                    text: '{avg_pct:.2f}%'
                    halign: 'center'
                    valign: 'middle'
                    font_size: '36sp'
        AsyncImage:
            id: unused_sheet_graph
            size_hint: (1, 1)
            source: f'http://{getenv("MY_IP")}:9034/graph-unused'
            nocache: True
        MDRaisedButton:
            text: 'Back'
            pos_hint: {{"center_x": .5, "center_y": .5}}
            on_release: app.fetch(f'http://{getenv('MY_IP')}:9034/kivy/camera', on_success=lambda _req, _res: app.screen_manager.replace_screen(_res["contents"]))
        MDRaisedButton:
            text: 'Graph example'
            pos_hint: {{"center_x": .5, "center_y": .5}}
            on_release: app.fetch(f'http://{getenv('MY_IP')}:9034/kivy/graph-example', on_success=lambda _req, _res: app.screen_manager.replace_screen(_res["contents"]))
        """,
        "name": "metal_waste_dashboard",
    })


@bp.route("/kivy/graph-example", methods=["GET"])
def kivy_graph_example():
    sheets = Sheet.query.all() or []
    sheets = sheets[-30:]  # take last 30 sheets

    return jsonify({
        "contents": f"""
Screen:
    BoxLayout:
        orientation: 'vertical'
        AsyncImage:
            id: unused_sheet_graph
            size_hint: (1, 1)
            source: f'http://{getenv("MY_IP")}:9034/graph-unused'
            nocache: True
            on_touch_up: setattr(touch_pos, 'text', f'x: {{args[1].x}}, y: {{args[1].y}}')
        MDLabel:
            id: touch_pos
            text: 'No touch'
        MDRaisedButton:
            text: 'Dashboard'
            on_release: app.fetch('http://{getenv("MY_IP")}:9034/kivy/dashboard', on_success=lambda req, resp: app.screen_manager.replace_screen(resp['contents']))
""",
        "name": "kivy_graph_example",
    })


@bp.route("/kivy/sheets", methods=["GET"])
def kivy_sheets():
    """
    """

    sheets = Sheet.query.order_by(Sheet.id.desc()).all()

    return jsonify({
        "contents": f"""
Screen:
    name: 'metal_waste_sheets'
    BoxLayout:
        padding: '20dp'
        spacing: '10sp'
        orientation: 'vertical'
        MDLabel:
            text: 'All Sheets'
            halign: 'left'
            valign: 'middle'
            font_size: '36sp'
            size_hint: 1, None
        GridLayout:
            padding: '10dp'
            spacing: '10dp'
            row_force_default: True
            row_default_height: 300
            cols: 2
            MDCard:
                BoxLayout:
                    orientation: 'vertical'
                    BoxLayout:
                        orientation: 'horizontal'
                        padding: '8dp'
                        MDLabel:
                            padding: '8dp'
                            size_hint: None, None
                            size: 3 * dp(48), dp(48)
                            font_style: 'Title'
                            theme_text_color: 'Primary'
                            text: 'N_065'
                    MDSeparator:
                        height: dp(1)
                    BoxLayout:
                        orientation: 'vertical'
                        AsyncImage:
                            source: 'http://{getenv("MY_IP")}:9034/static/images/N_065.jpg'
                        # MDLabel:
                        #     size_hint_y: None
                        #     height: int(self.parent.height * 0.9)
                        #     font_style: 'Body1'
                        #     theme_text_color: 'Primary'
                        #     text: 'Logs the start-to-finish process of a given work order.'
                        MDFlatButton:
                            text: 'view'
                            pos_hint: {{"center_x": .8}}
                            on_release: app.fetch('http://{getenv("MY_IP")}:9034/kivy/results/N_065', on_success=lambda req, resp: app.screen_manager.replace_screen(resp['contents']))
            MDCard:
                BoxLayout:
                    orientation: 'vertical'
                    BoxLayout:
                        orientation: 'horizontal'
                        padding: '8dp'
                        MDLabel:
                            size_hint: None, None
                            size: 3 * dp(48), dp(48)
                            font_style: 'Title'
                            theme_text_color: 'Primary'
                            text: 'N_065'
                    MDSeparator:
                        height: dp(1)
                    BoxLayout:
                        orientation: 'vertical'
                        AsyncImage:
                            source: 'http://{getenv("MY_IP")}:9034/static/images/N_065.jpg'
                        # MDLabel:
                        #     size_hint_y: None
                        #     height: int(self.parent.height * 0.9)
                        #     font_style: 'Body1'
                        #     theme_text_color: 'Primary'
                        #     text: 'Logs the start-to-finish process of a given work order.'
                        MDFlatButton:
                            text: 'view'
                            pos_hint: {{"center_x": .8}}
                            on_release: app.fetch('http://{getenv("MY_IP")}:9034/kivy/results/N_065', on_success=lambda req, resp: app.screen_manager.replace_screen(resp['contents']))
""",
        "name": "metal_waste_sheets"
    })


@bp.route("/kivy/record/<filename>", methods=["GET"])
def kivy_record_popup(filename):
    # source: 'http://{getenv('MY_IP')}:9034/static/images/tmp/{filename}'
    return jsonify({
        "contents": f"""
Popup:
    title: 'Enter Sheet Info'
    id: sheet_info_popup
    BoxLayout:
        orientation: 'vertical'
        AsyncImage:
            source: 'http://{getenv('MY_IP')}:9034/image/tmp/small/{filename}'
        GridLayout:
            cols: 4
            row_force_default: True
            row_default_height: 60
            TextInput:
                hint_text: 'Nest #'
                multiline: False
                id: nest_input
            TextInput:
                hint_text: 'Weight'
                multiline: False
                id: weight_input
            TextInput:
                hint_text: 'Initials'
                multiline: False
                id: initials_input
            Spinner:
                id: metal_type_spinner
                pos_hint: {{'center': (.5, .5)}}
                text: 'alum_080'
                values: 'alum_080', 'alum_063'
                on_text: print(self.text)
        GridLayout:
            cols: 2
            row_force_default: True
            row_default_height: 80
            Button:
                text: 'Discard'
                on_release: app.fetch('http://{getenv('MY_IP')}:9034/discard?filename={filename}', on_success=sheet_info_popup.dismiss(), method="POST")
            Button:
                text: 'Record'
                on_release: sheet_info_popup.dismiss()
                on_release: app.fetch(\
                'http://{getenv('MY_IP')}:9034/record',\
                data_type='form',\
                data={{ 'filename': '{filename}', 'serial_number': nest_input.text, 'initials': initials_input.text, 'weight': weight_input.text, 'metal_type': metal_type_spinner.text }},\
                on_success=lambda req, res: app.fetch(f'http://{getenv('MY_IP')}:9034/kivy/results/{{res["serial_number"]}}', on_success=lambda _req, _res: app.screen_manager.load_screens([_res["contents"]], next_screen=True)),\
                method="POST"\
                )
""",
        "name": "metal_waste_record"
    })


@bp.route("/kivy/results/<serial_number>", methods=["GET"])
def kivy_results_screen(serial_number):
    if serial_number == "last":
        s = Sheet.query.order_by(Sheet.id.desc()).first() or abort(404)
    else:
        s = Sheet.query.filter_by(serial_number=serial_number).first() or abort(404)
    prev = Sheet.query.order_by(Sheet.id.desc()).filter(Sheet.id < s.id).first() or None
    next = Sheet.query.order_by(Sheet.id.desc()).filter(Sheet.id > s.id).first() or None

    pprint(dict(s))

    return jsonify({
        "contents": f"""
Screen:
    name: 'metal_waste_results'
    BoxLayout:
        padding: '10sp'
        spacing: '10sp'
        orientation: 'vertical'
        GridLayout:
            cols: 2
            row_force_default: True
            row_default_height: 60
            MDRaisedButton:
                text: 'Previous'
                pos_hint: {{"center_x": .5}}
                on_release:
                    app.fetch('http://{getenv('MY_IP')}:9034/kivy/results/{prev.serial_number if prev is not None else 'last'}', \
                    on_success=lambda req, res: app.screen_manager.replace_screen(res['contents']))
            MDRaisedButton:
                text: "Next"
                pos_hint: {{"center_x": .5}}
                on_release:
                    app.fetch("http://{getenv('MY_IP')}:9034/kivy/results/{next.serial_number if next is not None else 'last'}", \
                    on_success=lambda req, res: app.screen_manager.replace_screen(res["contents"]))
        GridLayout:
            cols: 2
            row_force_default: True
            row_default_height: 60
            MDLabel:
                text: '{s.serial_number} Results'
                valign: 'middle'
                halign: 'center'
            MDLabel:
                valign: 'middle'
                halign: 'center'
                color: [1, 0, 0, 1] if {int(s.calc_unused_pct)} >= 20 else [0, 1, 0, 1]
                text: 'PASS' if {int(s.calc_unused_pct)} < 20 else 'FAIL'
                bold: True
        AsyncImage:
            source: 'http://{getenv('MY_IP')}:9034/static/images/{s.id}_sm.jpg'
        GridLayout:
            cols: 4
            row_force_default: True
            row_default_height: 60
            spacing: [20, 5]
            # row 1
            MDLabel:
                text: 'Weight'
                halign: 'left'
                text_size: self.size
                valign: 'middle'
            MDLabel:
                text: '{s.weight} lbs / {weight_factors[s.metal_type]} lbs'
                halign: 'right'
                text_size: self.size
                valign: 'middle'
            MDLabel:
                text: 'Cut Date'
                halign: 'left'
                text_size: self.size
                valign: 'middle'
            MDLabel:
                text: '{str(s.cut_date)[:10]}'
                halign: 'right'
                text_size: self.size
                valign: 'middle'
            # row 2
            MDLabel:
                text: 'Exp. Unused'
                halign: 'left'
                text_size: self.size
                valign: 'middle'
            MDLabel:
                text: '{s.exp_unused_pct:.2f}%'
                halign: 'right'
                text_size: self.size
                valign: 'middle'
            MDLabel:
                text: 'Calc. Unused'
                halign: 'left'
                text_size: self.size
                valign: 'middle'
            MDLabel:
                text: '{s.calc_unused_pct:.2f}%'
                halign: 'right'
                text_size: self.size
                valign: 'middle'
                color: [1, 0, 0, 1] if {int(s.calc_unused_pct)} >= 20 else [0, 1, 0, 1]
            # row 3
            MDLabel:
                text: 'Total Area'
                halign: 'left'
                text_size: self.size
                valign: 'middle'
            MDLabel:
                text: '{s.total_area:.2f} sq.in.'
                halign: 'right'
                text_size: self.size
                valign: 'middle'
            MDLabel:
                text: 'Used Area'
                halign: 'left'
                text_size: self.size
                valign: 'middle'
            MDLabel:
                text: '{s.used_area:.2f} sq.in.'
                halign: 'right'
                text_size: self.size
                valign: 'middle'
            # row 4
            MDLabel:
                text: 'Unused Area'
                halign: 'left'
                text_size: self.size
                valign: 'middle'
            MDLabel:
                text: '{s.unused_area:.2f} sq.in.'
                halign: 'right'
                text_size: self.size
                valign: 'middle'
            MDLabel:
                text: 'Unused Delta (weight / area)'
                halign: 'left'
                text_size: self.size
                valign: 'middle'
            MDLabel:
                text: '{abs(s.exp_unused_pct - s.calc_unused_pct):.2f}'
                halign: 'right'
                text_size: self.size
                valign: 'middle'
        MDRaisedButton:
            text: 'Back to Camera'
            pos_hint: {{"center_x": .5}}
            on_release:
                app.fetch('http://{getenv('MY_IP')}:9034/kivy/camera', \
                on_success=lambda req, res: app.screen_manager.replace_screen(res['contents']))
""",
        "name": "metal_waste_results"
    })
