from flask import redirect, abort, Blueprint, Response, current_app as app
import cv2
import os
import app.image_proc as ip

bp = Blueprint("image", __name__)


IP_CAM_URL = 'http://126.10.240.20:8081/'


@bp.route('/image')
def image_index():
    return '<img src="/image/feed"></img>'


@bp.route('/image/feed')
def feed():
    return Response(
        gen_frames(0.15),
        mimetype='multipart/x-mixed-replace; boundary=frame')


@bp.route('/image/tmp/<string:size>/<string:filename>')
def tmp_image_scale(size, filename):
    filepath = os.path.join(
        app.config['STATIC_FOLDER'], 'images', 'tmp', filename)
    print('full filepath', filepath, flush=True)
    if not os.path.exists(filepath):
        print('filepath does not exist. abort', flush=True)
        abort(404)
    image = cv2.imread(filepath)
    if size == 'small':
        image = ip.scale_image(image, 0.1)
    elif size == 'medium':
        image = ip.scale_image(image, 0.5)
    print('return image', flush=True)
    succ, buffer = cv2.imencode('.jpg', image)
    return Response(buffer.tobytes(), mimetype='image/jpeg')


@bp.route('/image/still/<string:size>')
def still(size):
    succ, image = get_image()
    if not succ:
        abort(500)
    image = ip.debug_image(image)
    if size == 'small':
        print('return small image', flush=True)
        image = ip.scale_image(image, 0.2)
    elif size == 'medium':
        print('return medium image', flush=True)
        image = ip.scale_image(image, 0.3)
    succ, buffer = cv2.imencode('.jpg', image)
    return Response(buffer.tobytes(), mimetype='image/jpeg')


# UTILS
def to_frame(image, scale=1):
    image = ip.scale_image(image, scale)
    ret, buffer = cv2.imencode('.jpg', image)
    image = buffer.tobytes()
    # concat frame one by one and show result
    return (
        b'--frame\r\n'
        b'Content-Type: image/jpeg\r\n\r\n' + image + b'\r\n')


def get_image():
    camera = cv2.VideoCapture(IP_CAM_URL)
    ret = camera.read()
    camera.release()
    return ret


def gen_frames(scale, calibration=None, samples=3):
    camera = cv2.VideoCapture(IP_CAM_URL)
    while True:
        succ, image = camera.read()
        if not succ:
            break
        image = ip.debug_image(image)
        yield to_frame(image, scale)
    camera.release()
