#!/bin/bash

function get_dir() {
   SOURCE="${BASH_SOURCE[0]}"
   while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
     DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
     SOURCE="$(readlink "$SOURCE")"
     [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
   done
   DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
   echo $DIR
}

source ~/anaconda3/etc/profile.d/conda.sh
conda activate aluminum_waste
curr_dir=$(get_dir)
echo $curr_dir
cd $curr_dir

# this is not dumb, should have apache serve
python main.py
